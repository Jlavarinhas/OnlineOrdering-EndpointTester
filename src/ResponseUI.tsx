import React, { useState } from "react";
import { ApiClient } from "./ApiClient";
import Response from "./Response";
import { Client, Order } from "./OOApi";
import { feature, hostname, master, diffBody } from "./api.constants";
import { sendOrderRequest } from "./request/sendOrder";

import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import { TextField } from "@mui/material";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export type ResponseProps = {
  label: string;
};

export default function ResponseUI({ label }: ResponseProps) {
  const [allStores, setAllStores] = useState<object | undefined>(undefined);
  const [guestOrder, setGuestOrder] = useState<object | undefined>(undefined);
  const [expanded, setExpanded] = React.useState<string | false>(false);

  const handleChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  return (
    <>
      <Accordion onChange={handleChange("panel1")}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography sx={{ width: "33%", flexShrink: 0 }}>
            PointOne.OnlineOrdering.Api
          </Typography>
          <Typography sx={{ color: "text.secondary" }}>{label}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <Box sx={{ flexGrow: 1 }}>
              <Grid container spacing={2} columns={16}>
                <Grid xs={8}>
                  <Item>
                    <Response
                      label={master}
                      value={allStores}
                      func={() => {
                        let response: object | null = null;
                        new Client(hostname, ApiClient)
                          .getAllStores(
                            "B039C4F7-C83D-451D-B7E9-1E2EACEAEA6E",
                            "AB9A3F1A-93A1-4DF1-81FA-C5850031514F"
                          )
                          .then((r) => {
                            response = r;
                            setAllStores(r);
                          })
                          .catch((r) => {
                            console.log(1, r);
                          });
                        return response;
                      }}
                    />
                  </Item>
                </Grid>
                <Grid xs={8}>
                  <Item>
                    <Response
                      label={feature}
                      value={allStores}
                      func={() => {
                        let response: object | null = null;
                        new Client(hostname, ApiClient)
                          .getAllStores(
                            "B039C4F7-C83D-451D-B7E9-1E2EACEAEA6E",
                            "AB9A3F1A-93A1-4DF1-81FA-C5850031514F"
                          )
                          .then((r) => {
                            response = r;
                            setAllStores(r);
                          })
                          .catch((r) => {
                            console.log(1, r);
                          });
                        return response;
                      }}
                    />
                  </Item>
                </Grid>
                <Grid item xs={16}>
                  <Item>
                    <TextField
                      id="outlined-multiline-flexible"
                      label="diff"
                      variant="outlined"
                      multiline
                      fullWidth={true}
                      rows={5}
                      value={diffBody}
                    />
                  </Item>
                </Grid>
              </Grid>
            </Box>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion onChange={handleChange("panel1")}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography sx={{ width: "33%", flexShrink: 0 }}>
            PointOne.OnlineOrdering.Api
          </Typography>
          <Typography sx={{ color: "text.secondary" }}>
            {"/GuestOrder/JSON"}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <Box sx={{ flexGrow: 1 }}>
              <Grid container spacing={2} columns={16}>
                <Grid xs={8}>
                  <Item>
                    <Response
                      label={master}
                      value={guestOrder}
                      func={async () => {
                        let response: object | null | undefined = null;
                        let order: Order = JSON.parse(sendOrderRequest);
                        new Client(hostname, ApiClient)
                          .getGuestOrder(order)
                          .then((r2) => {
                            response = r2;
                            setGuestOrder(r2);
                          })
                          .catch((r2) => {
                            console.log(1, r2);
                          })
                          .catch((ex) => console.log(2, "error", ex));
                        return response;
                      }}
                    />{" "}
                  </Item>
                </Grid>
                <Grid xs={8}>
                  <Item>
                    <Response
                      label={feature}
                      value={guestOrder}
                      func={async () => {
                        let response: object | null | undefined = null;
                        let order: Order = JSON.parse(sendOrderRequest);
                        new Client(hostname, ApiClient)
                          .getGuestOrder(order)
                          .then((r2) => {
                            response = r2;
                            setGuestOrder(r2);
                          })
                          .catch((r2) => {
                            console.log(1, r2);
                          })
                          .catch((ex) => console.log(2, "error", ex));
                        return response;
                      }}
                    />{" "}
                  </Item>
                </Grid>
                <Grid item xs={16}>
                  <Item>
                    <TextField
                      id="outlined-multiline-flexible"
                      label="diff"
                      variant="outlined"
                      multiline
                      fullWidth={true}
                      rows={5}
                      value={diffBody}
                    />
                  </Item>
                </Grid>
              </Grid>
            </Box>
          </Typography>
        </AccordionDetails>
      </Accordion>
    </>
  );
}
