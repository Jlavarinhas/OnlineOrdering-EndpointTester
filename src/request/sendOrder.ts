export const sendOrderRequest: string = `{
  "APIKey": "B039C4F7-C83D-451D-B7E9-1E2EACEAEA6E",
  "SiteID": "F9837249-0FF7-42A9-BE0D-D5553ADC1FF2",
  "UserID": "",
  "OrderItems": [
    {
      "ID": "{BDF67429-BC1A-41B2-AD95-4244C85296C4}",
      "Quantity": 1,
      "ModifierOptions": [],
      "ItemName": null,
      "ItemPrice": 0,
      "VATRate": 0,
      "HasModifiers": false,
      "HasImage": false,
      "Thumbnail": null,
      "DoubleMeasure": false,
      "MiscItem": false,
      "OpenItemID": null,
      "DiscountedPrice": 0,
      "ItemMessage": null,
      "OverridePrice": 6,
      "IntervalNumber": 0
    }
  ],
  "OrderType": 2,
  "RequiredDate": "30/08/2022",
  "RequiredTime": "20:00",
  "Name": "TEST ORDER DONT PREP",
  "TableID": "11",
  "ServiceCharge": 4.96,
  "DeliveryAddress": "",
  "DeliveryPostcode": "",
  "TelNo": "03458620005",
  "Email": "jason.lavarinhas@pointone.co.uk",
  "DeliveryInstructions": "",
  "DeliveryCost": 0,
  "PromoCode": "",
  "Payment": {
    "PaymentType": 0,
    "CardType": null,
    "CardNo": null,
    "CardStartDate": null,
    "CardExpiryDate": null,
    "NameOnCard": null,
    "CardCVV": null,
    "CardAddress": null,
    "CardPostcode": null,
    "TransactionID": null,
    "RedirectURL": null,
    "PaypalAuthURL": null,
    "PaypalCancelURL": null,
    "JudoReceiptID": null,
    "JudoCustomerToken": null
  },
  "OrderTotal": 6,
  "PromoCodeDiscount": 0,
  "Discount": 0,
  "DiscountReason": "App Discount",
  "OperationMode": 0,
  "InternalTransactionID": "6885d8ae-1ece-4386-af78-3dbea62bb5af",
  "AppVersion": null,
  "BillID": 0,
  "TakeawayID": null,
  "OrderSource": 0,
  "CalculatedTotal": 0,
  "AssociatedPromoCode": null,
  "Deposits": null,
  "DeviceName": null,
  "Como": null,
  "IntervalName": null,
  "IntervalPickupLocation": 0,
  "DeferPrint": false,
  "Gratuity": 0,
  "IntervalPickupID": 0,
  "SeatID": 0,
  "DontValidatePrepTime": true,
  "VatNumber": null
}`;
