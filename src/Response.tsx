import { useState, useEffect } from "react";
import { TextField } from "@mui/material";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import { CustomFunction } from "./CustomFunction";
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export type ResponseProps = {
  label: string;
  value: object | undefined;
  func: CustomFunction;
};

// let myAdd = function (x: number, y: number): object {
//   return {};
// };

export default function Response({ label, func, value }: ResponseProps) {
  const [response, setResponse] = useState<object | null | undefined>(null);
  async function fetchData() {
    let data = func();
    setResponse(data);
  }

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <Grid item xs={16}>
        <Item>
          <TextField
            id="outlined-multiline-flexible"
            label={label}
            variant="outlined"
            multiline
            fullWidth={true}
            rows={24}
            value={JSON.stringify(value, null, 4)}
          />
        </Item>
      </Grid>
    </>
  );
}
