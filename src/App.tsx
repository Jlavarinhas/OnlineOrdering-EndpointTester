import ResponseUI from "./ResponseUI";
function App() {
  return (
    <div className="App">
      <ResponseUI label="/Stores/JSON/{APIKey}/{CompanyID}" />
    </div>
  );
}

export default App;
