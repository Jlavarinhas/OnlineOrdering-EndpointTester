import  axios from "axios";
export const ApiClient = axios.create();
ApiClient.interceptors.request.use((config) => {
    config.baseURL = "https://localhost:6732/" ;
    config.headers =  {'Content-Type': 'application/json'}
    return config;
});